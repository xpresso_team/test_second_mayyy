import python_pachyderm as pachyderm

from xpresso.ai.core.commons.utils.xpr_config_parser import XprConfigParser
from xpresso.ai.core.commons.exceptions.xpr_exceptions\
    import PachydermConnectionException, PachydermServerException
from grpc._channel import _Rendezvous as PachydermCmdException, \
    _InactiveRpcError as PachydermServiceException


class PachydermConnector:
    """
    class that creates a connection instance to a pachyderm cluster
    """

    def __init__(self, server_host, server_port):
        self.host = server_host
        self.port = server_port
        print(server_host, server_port)
        self.check_server(server_host, server_port)

    @staticmethod
    def check_server(host, port):
        """
        checks if the pachyderm api_server details provided are valid

        calls get_remote_version method in pachyderm module to
        cross check if a pachyderm cluster is setup on the host
        and throws exception in case of invalid host/port combination

        :param host:
            ip address of the host api_server
        :param port:
            port on which the pachyderm daemon is running
        :returns :
        """
        try:
            pachyderm.get_remote_version(host, port)
        except (PachydermServiceException, PachydermCmdException) as err:
            raise PachydermServerException(err.details())

    def connect(self):
        """
        starts a new connection with pachyderm cluster

        creates a new Pfs(pachyderm file system) channel and
        connects to the cluster

        :return:
            returns a PfsClient object
        """
        try:
            client = pachyderm.PfsClient(self.host, self.port)
            return client
        except PachydermCmdException as err:
            raise PachydermConnectionException(err.details())
