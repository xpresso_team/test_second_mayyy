import pika,json
from abc import ABCMeta
from xpresso.ai.core.model_monitoring.message_queue.abstract_message_queue_manager import \
    AbstractMessageQueueManager
from xpresso.ai.core.commons.utils.constants import BLANK, MODEL_MONITORING_SECTION, \
    RABBIT_MQ_SECTION, RABBIT_MQ_HOST, RABBIT_MQ_PORT, MESSAGE_QUEUE_SECTION
from xpresso.ai.core.commons.utils.xpr_config_parser import XprConfigParser


class RabbitMQManager(AbstractMessageQueueManager,metaclass=ABCMeta):

    EXCHANGE=BLANK
    TYPE='fanout'
    ROUTING_KEY = 'xpr'
    QUEUE = "xpr"

    def __init__(self):
        super().__init__()
        self.config = XprConfigParser()
        self._params = pika.ConnectionParameters(
            host=self.config[
                MODEL_MONITORING_SECTION][MESSAGE_QUEUE_SECTION][RABBIT_MQ_SECTION][RABBIT_MQ_HOST],
            port=self.config[
                MODEL_MONITORING_SECTION][MESSAGE_QUEUE_SECTION][RABBIT_MQ_SECTION][RABBIT_MQ_PORT])
        self._conn = None
        self._channel = None

    def connect(self):
        if not self._conn or self._conn.is_closed:
            self._conn = pika.BlockingConnection(self._params)
            self._channel = self._conn.channel()
            self._channel.queue_declare(queue=self.QUEUE)

    def _publish(self, msg, headers):
        properties = pika.BasicProperties(content_type='application/json',
                                          headers=headers)

        self._channel.basic_publish(exchange=self.EXCHANGE,
                                    routing_key=self.ROUTING_KEY,
                                    body=json.dumps(msg),
                                    properties=properties)

        print('message sent: %s', msg)

    def publish(self, msg=None,headers=None):
        """Publish msg, reconnecting if necessary."""
        if self._channel is None:
            self.connect()

        try:
            self._publish(msg,headers=headers)
        except pika.exceptions.ConnectionClosed:
            print('reconnecting to queue')
            self.connect()
            self._publish(msg)

    def close(self):
        if self._conn and self._conn.is_open:
            print('closing queue connection')
            self._conn.close()

    def consume(self, method=None):
        if self._channel is None:
            self.connect()

        print("Starting Consumer")
        self._channel.basic_consume(queue=self.QUEUE,
                                    on_message_callback=method, auto_ack=True)

        self._channel.start_consuming()